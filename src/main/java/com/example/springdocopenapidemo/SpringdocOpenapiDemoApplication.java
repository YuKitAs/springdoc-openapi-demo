package com.example.springdocopenapidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringdocOpenapiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringdocOpenapiDemoApplication.class, args);
	}

}
