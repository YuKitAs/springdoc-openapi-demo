package com.example.springdocopenapidemo.config;

import com.example.springdocopenapidemo.model.StringValueObject;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.SpringDocUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {
    static {
        SpringDocUtils.getConfig().replaceWithClass(StringValueObject.class, String.class);
    }

    @Bean
    public OpenAPI springOpenAPI() {
        return new OpenAPI().info(new Info().title("Demo"));
    }
}
