package com.example.springdocopenapidemo.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(name = "String VO", description = "some desc", example = "some example")
public class StringValueObject {
    private String value;

    public String getValue() {
        return value;
    }
}
