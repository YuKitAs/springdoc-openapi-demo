package com.example.springdocopenapidemo.controller.dto;

import com.example.springdocopenapidemo.model.StringValueObject;
import io.swagger.v3.oas.annotations.media.Schema;

public class DemoRequest {
    @Schema(description = "other desc", example = "other example")
    private StringValueObject name;

    public StringValueObject getName() {
        return name;
    }
}
