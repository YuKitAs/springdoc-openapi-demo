package com.example.springdocopenapidemo.controller;

import com.example.springdocopenapidemo.controller.dto.DemoRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/hello")
public class DemoController {

    @PostMapping
    public String hello(@RequestBody DemoRequest request) {
        return "Hello " + request.getName().getValue();
    }

}
